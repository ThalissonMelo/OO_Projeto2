package Imagempackage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.util.Scanner;
import javax.swing.event.TableModelEvent;
import static oracle.jrockit.jfr.events.Bits.length;




public class EncontraMensagem {
    private Object Systen;
    private int posicaoInicial;
    private String mensagem;

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public int getPosicaoInicial() {
        return posicaoInicial;
    }
 
    public EncontraMensagem(String nomeDoArquivo) throws FileNotFoundException, IOException{
        Imagem imagem1 = new Imagem(nomeDoArquivo);
        

        int contador = 0;
        int byteRetirado=0;
        int novoCaracter=0;
        contador=1;
        String mensagem = new String();
        do{
        
        byteRetirado =  (int)(imagem1.getImagemCopia()[imagem1.getInitialPosition()+contador-1]) & 0x01;
        
        if(contador%8==0){
        novoCaracter = (byteRetirado) | (novoCaracter<<1);
        
        if((char)novoCaracter == '#'){
            break;
        }
        mensagem+=(char)novoCaracter;
        //System.out.print((char)novoCaracter);
        novoCaracter = 0;
        }
        
        else{
        novoCaracter = (byteRetirado) | (novoCaracter<<1);
        }
        contador++;
        }while((char)novoCaracter != '#');
        
        setMensagem(mensagem);
    //System.out.println(mensagem);
    }
    
}
