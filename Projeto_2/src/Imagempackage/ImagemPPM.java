package Imagempackage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author thalisson
 */
public class ImagemPPM {
    private byte[] CopiaPPM;
    private byte[] FiltroPPM;
    private int escolha;

    public int getEscolha() {
        return escolha;
    }

    public void setEscolha(int escolha) {
        this.escolha = escolha;
    }

    public byte[] getFiltroPPM() {
        return FiltroPPM;
    }

    public void setFiltroPPM(byte[] FiltroPPM) {
        this.FiltroPPM = FiltroPPM;
    }

    public byte[] getCopiaPPM() {
        return CopiaPPM;
    }

    public void setCopiaPPM(byte[] CopiaPPM) {
        this.CopiaPPM = CopiaPPM;
    }

    public ImagemPPM(String nomeDoArquivo, int filtro2) throws FileNotFoundException, IOException{
       
        Imagem imagem = new Imagem(nomeDoArquivo);
        byte[] imagemCopia = new byte[imagem.getColunas()*imagem.getLinhas()*3];
        byte[] filtro = new byte[imagem.getColunas()*imagem.getLinhas()*3+imagem.getTudoJunto().length()];
        int contador = 0;
        FileInputStream imagemPGM = new  FileInputStream(nomeDoArquivo);
        imagemPGM.skip(imagem.getTudoJunto().length());
        while(contador<(imagem.getColunas()*imagem.getLinhas()*3)){
           imagemCopia[contador] = (byte) imagemPGM.read();   
        contador++;
        }
        setCopiaPPM(imagemCopia);
        
        contador=0;
        ///setEscolha();
        int count=0;
        
                byte[] copy = new byte[imagem.getColunas()*imagem.getLinhas()*3];

                while(count<(imagem.getColunas()*imagem.getLinhas()*3))
                {
                    for(int j=1;j<=3;j++)
                    {
                        if(j!=filtro2)
                        {
                            copy[count]=0;
                        }
                        else
                        {
                            copy[count]=(imagemCopia[count]);
                        }

                        count++;	
                    }
                    
                }
                 setFiltroPPM(copy);
                FileOutputStream arq = new FileOutputStream("saida.pgm");
            arq.write(imagem.getTudoJunto().getBytes());
            arq.write(getFiltroPPM());
                /*char[] auxiliar = new char[imagem.getTudoJunto().length()];
                for(int k = 0; k<imagem.getTudoJunto().length(); k++)
                auxiliar[k] = imagem.getTudoJunto().toCharArray()[k];
                
                
                //byte[] imagemCompleta = new byte[imagem.getTudoJunto().length()+imagem.getColunas()*imagem.getLinhas()];
                int i=0;
                for(int k = 0; k<imagem.getTudoJunto().length(); k++){
                filtro[k] = (byte) auxiliar[k];
                i++;
                }
                for(int k=i ; k<imagem.getColunas()*imagem.getLinhas()*3; k++){
                filtro[k] = copy[k];
                }*/
                
               
                
    }
}
